package com.example.chapter3challengefauzi

import android.content.ActivityNotFoundException
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.chapter3challengefauzi.databinding.FragmentDetailMenuBinding
import java.lang.NullPointerException

class DetailMenuFragment : Fragment(){

    private var _binding: FragmentDetailMenuBinding? = null
    private val binding get() = _binding!!

    private val location: String = "https://maps.app.goo.gl/h4wQKqaBuXzftGK77"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentDetailMenuBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        try {
            val dataFragmentHome = DetailMenuFragmentArgs.fromBundle(arguments as Bundle)
            binding.imageDetailMenu.setImageResource(dataFragmentHome.imageDetailMenu)
            binding.namaDetailMenu.text = dataFragmentHome.nameDetailMenu
            binding.priceDetailMenu.text = dataFragmentHome.priceDetaiMenu

            binding.location.setOnClickListener{
                try {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse(location))
                    startActivity(intent)
                } catch (e:ActivityNotFoundException){
                    Toast.makeText(
                        requireContext(),
                        "Google Maps tidak terinstall",
                        Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }catch (e:NullPointerException){
            Toast.makeText(
                requireContext(),
                "Error: $e", Toast.LENGTH_SHORT)
                .show()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}
