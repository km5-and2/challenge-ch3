package com.example.chapter3challengefauzi

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.chapter3challengefauzi.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding : FragmentHomeBinding? = null
    private val binding get() = _binding!!

    private var isListView = true
    private var typeLayout = true

    private val listMakanan = arrayListOf<ListItemMenu>()
    private val icon = arrayListOf(
        R.drawable.baseline_format_list_bulleted_24,
        R.drawable.baseline_border_all_24
    )

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?,
        ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentHomeBinding.inflate(inflater, container, false)

        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val toggleButton = binding.buttonList

        toggleButton.setOnClickListener{
            isListView = !isListView
            toggleRV()
            toggleImageView(toggleButton)
        }
        toggleRV()
    }


    private fun showRecycleMakanan(){
        binding.recyclerMakanan.layoutManager = GridLayoutManager(requireActivity(), 2)
        val menuAdapater = AdapterMenu(listMakanan)
        binding.recyclerMakanan.adapter = menuAdapater
    }

    private fun toggleImageView(imageView: ImageView){
        imageView.setImageResource(icon[if(isListView)0 else 1])
    }

    private fun gridLayout(){
        binding.recyclerMakanan.layoutManager = GridLayoutManager(requireActivity(), 2)
        val adapterMenu = AdapterMenu(listMakanan, gridLayout = true)
        binding.recyclerMakanan.adapter = adapterMenu
    }

    private fun linearLayout(){
        binding.recyclerMakanan.layoutManager = LinearLayoutManager(requireActivity())
        val adapterMenu = AdapterMenu(listMakanan, gridLayout = false)
        binding.recyclerMakanan.adapter = adapterMenu
    }

    private fun toggleRV(){
        listMakanan.clear()

        if(isListView){
            gridLayout()
            typeLayout = true
        }else{
            linearLayout()
            typeLayout=false
        }

        val adapter = AdapterMenu(listMakanan, gridLayout = typeLayout, onItemClick = {selectedItem ->

            val toDetailMenu = HomeFragmentDirections.actionHomeFragmentToDetailMenuFragment()

                toDetailMenu.nameDetailMenu = selectedItem.namaMakanan
                toDetailMenu.priceDetaiMenu = selectedItem.harga
                toDetailMenu.imageDetailMenu = selectedItem.image

            findNavController().navigate(toDetailMenu)
        })

        binding.recyclerMakanan.adapter = adapter

        listMakanan.add(ListItemMenu(R.drawable.burger,"Cheese Burger", "Rp.50.000" ))
        listMakanan.add(ListItemMenu(R.drawable.panggang,"Ayam Panggang", "Rp.50.000"))
        listMakanan.add(ListItemMenu(R.drawable.sate,"Sate Ayam", "Rp.25.000"))
        listMakanan.add(ListItemMenu(R.drawable.sushi,"Sushi", "Rp.40.000"))
        listMakanan.add(ListItemMenu(R.drawable.pasta,"Pasta", "Rp.40.000"))
        listMakanan.add(ListItemMenu(R.drawable.dimsum,"Dimsum", "Rp.30.000"))
    }

}