package com.example.chapter3challengefauzi

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class AdapterMenu (
    private val datalistMenu: ArrayList<ListItemMenu>,
    private val gridLayout: Boolean = true,
    var onItemClick: ((ListItemMenu) -> Unit)? = null
    ) : RecyclerView.Adapter<AdapterMenu.ViewHolder>(){

    //classViewHolder
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val gambar = itemView.findViewById<ImageView>(R.id.imageMenu)
        val namaMakanan = itemView.findViewById<TextView>(R.id.textNamaMakanan)
        val harga = itemView.findViewById<TextView>(R.id.textHarga)
    }

    //membuat ViewHolder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutResId = if(gridLayout)R.layout.grid_menu
                          else R.layout.linier_menu
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.linier_menu, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val (image, name, price) = datalistMenu[position]
        holder.gambar.setImageResource(image)
        holder.namaMakanan.text = name
        holder.harga.text = price

        val currentItem = datalistMenu[position]
        holder.itemView.setOnClickListener{
            onItemClick?.invoke(currentItem)
            Log.d("ItemClicked", "Item clicked {$currentItem}")
        }
    }

    override fun getItemCount(): Int {
        return datalistMenu.size
    }
}